<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
          'role_id' => 1,
          'name' => 'Admin',
          'email' => 'admin@gmail.com',
          'password' => Hash::make('password')
      ]);

      DB::table('users')->insert([
          'role_id' => 2,
          'name' => 'Medewerker',
          'email' => 'medewerker@gmail.com',
          'password' => Hash::make('password')
      ]);

      DB::table('users')->insert([
          'role_id' => 3,
          'name' => 'Student',
          'email' => 'student@gmail.com',
          'password' => Hash::make('password')
      ]);
    }
}
