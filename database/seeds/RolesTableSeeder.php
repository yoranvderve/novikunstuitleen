<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'Admin',
            'value' => 'admin',
        ]);

        DB::table('roles')->insert([
            'name' => 'Employee',
            'value' => 'employee',
        ]);

        DB::table('roles')->insert([
            'name' => 'Student',
            'value' => 'student',
        ]);
    }
}
