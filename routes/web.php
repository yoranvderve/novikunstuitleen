<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth'])->group(function () {

Route::get('logout',function(){
    Session::flush();
    Auth::logout();
    return redirect(route('login'));
});

Route::get('/', 'HomeController@index')->name('home');

Route::resource('user', 'UserController');

Route::resource('art','ArtController');
Route::delete('art-photo-delete/{artPhoto}', 'ArtController@deletePhoto')->name('art.photo.delete');
Route::get('order', 'OrderController@index')->name('order.index');

Route::get('student/art', 'Student\ArtController@index')->name('student.art.index');
Route::get('student/art/{art}', 'Student\ArtController@show')->name('student.art.show');

Route::get('rent-art/{art}', 'Student\ArtController@rent')->name('student.art.rent');
Route::post('rent-art/{art}', 'Student\ArtController@confirm')->name('student.art.confirm');

Route::get('order-success/{order}', 'Student\OrderController@success')->name('order.success');
Route::get('student/order', 'Student\OrderController@index')->name('student.order.index');
Route::get('send-back/{order}', 'Student\OrderController@sendBack')->name('student.order.send.back');
});
