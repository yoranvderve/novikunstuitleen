<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Art extends Model
{
    protected $fillable = [
      'name',
      'material',
      'description',
      'price',
      'status'
    ];

    public static function uploadPictures($art, $request)
    {
        if($request->hasFile('pictures')){
            foreach($request->allFiles()['pictures'] as $file){
                $name = $file->getClientOriginalName();
                $imageName = time().'_'.$name;
                $path = '/artwork-pictures/'.$art->id;
                if($file->move(public_path($path), $imageName)){
                    ArtPhoto::create(['art_id'=>$art->id,'name'=>$imageName,'path'=>$path]);
                }
            }
        }
    }

    public static function confirm($art, $request)
    {
        if($request->get('address_select') != null) {
          $address_id = $request->get('address_select');
        }
        if($request->get('street') != null && $request->get('street') != '') {
            $address = Auth::user()->addresses()->create([
                'street'=>$request->get('street'),
                'number'=>$request->get('number'),
                'suffix'=>$request->get('suffix'),
                'zip_code'=>$request->get('zip_code'),
                'city'=>$request->get('city'),
                'country'=>$request->get('country'),
            ]);
            $address_id = $address->id;

        }

        return Order::create([
            'user_id' => Auth::user()->id,
            'art_id' => $art->id,
            'address_id'=>$address_id,
            'amount_of_months' => $request->get('period_select'),
            'price' => (double) $art->price,
            'payment_id' => 'not_paid',
            'paid' => false,
        ]);
    }

    public function artPhotos()
    {
        return $this->hasMany(ArtPhoto::class);
    }

    public function orders()
    {
      return $this->hasMany(Order::class);
    }
}
