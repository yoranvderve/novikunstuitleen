<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Mollie\Laravel\Facades\Mollie;

class Order extends Model
{
    protected $fillable = [
        'user_id',
        'art_id',
        'address_id',
        'amount_of_months',
        'price',
        'payment_id',
        'paid',
        'send_back',
    ];

    public static function preparePayment($order)
    {
        $price = $order->price;
        if (floor($order->price) === $order->price) {
            $price = $order->price.'.00';
        }

        $payment = Mollie::api()->payments()->create([
        'amount' => [
            'currency' => 'EUR',
            'value' => (string) $price,
        ],
        'method' => "ideal",
        'description' => 'Art Rent NoviKunstuitleen',
        // 'webhookUrl' => route('webhooks.mollie'),
        'redirectUrl' => route('order.success', $order),
        ]);

        $payment = Mollie::api()->payments()->get($payment->id);

        $order->update(['payment_id' => $payment->id]);

        return $payment->getCheckoutUrl();
    }

    public function art()
    {
        return $this->belongsTo(Art::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }
}
