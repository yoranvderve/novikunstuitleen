<?php

namespace App\Http\Controllers\Student;

use App\Art;
use App\ArtPhoto;
use App\Order;
use App\Address;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ArtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = view('student.art.index');
        $view->artworks = Art::where('status','available')->get();
        return $view;
    }

    /**
     * Display the specified resource.
     *
     * @param  Art  $art
     * @return \Illuminate\Http\Response
     */
    public function show(Art $art)
    {
        $view = view('student.art.show');
        $view->art = $art;
        return $view;
    }

    /**
     * Display the specified resource to rent art.
     *
     * @param  Art  $art
     * @return \Illuminate\Http\Response
     */
    public function rent(Art $art)
    {
        $view = view('student.art.rent');
        $view->art = $art;
        $view->addresses = Auth::user()->addresses;
        return $view;
    }

    /**
     * Create the order
     *
     * @param  Art  $art
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function confirm(Request $request, Art $art)
    {

        $order = Art::confirm($art, $request);

        $redirectUrl = Order::preparePayment($order);
        return redirect($redirectUrl, 303);
    }
}
