<?php

namespace App\Http\Controllers\Student;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Mollie\Laravel\Facades\Mollie;

class OrderController extends Controller
{

    public function index()
    {
        $view = view('student.order.index');
        $view->orders = Auth::user()->orders()->where('paid', 1)->get();
        return $view;
    }

    public function sendBack(Order $order)
    {
        $order->update(['send_back'=>true]);
        $order->art->update(['status'=>'available']);
        return redirect()->back()->with('success', 'Art Item Successfully Send Back!');
    }

    public function success(Order $order)
    {
        $view = view('student.order.confirm');
        $payment = Mollie::api()->payments()->get($order->payment_id);

        $isPaid = $payment->isPaid();

        if ($isPaid) {
            $order->update(['paid'=>true]);
            $order->art()->update(['status'=>'rented']);
        }

        $view->order = $order;
        return $view;
    }
}
