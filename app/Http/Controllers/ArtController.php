<?php

namespace App\Http\Controllers;

use App\Art;
use App\ArtPhoto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ArtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = view('art.index');
        $view->artworks = Art::all();
        return $view;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view = view('art.create');

        return $view;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        if($data['price'] > 50000) {
            $data['price'] = 50000;
        }

        $art = Art::create($data);

        Art::uploadPictures($art, $request);

        return redirect()->route('art.index')->with('success', 'Artwork stored');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Art $art)
    {
        $view = view('art.edit');
        $view->art = $art;
        $view->photos = $art->artPhotos;
        return $view;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Art $art)
    {
        $data = $request->all();

        if($data['price'] > 50000) {
            $data['price'] = 50000;
        }

        $art->update($data);

        Art::uploadPictures($art, $request);
        
        return redirect(route('art.index'))->with('success', 'Successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Art $art)
    {
        $art->delete();
        return redirect()->back()->with('success', 'Artwork deleted');
    }

    public function deletePhoto(ArtPhoto $artPhoto)
    {
        $artPhoto->delete();
        return redirect()->back()->with('success', 'Photo deleted');
    }
}
