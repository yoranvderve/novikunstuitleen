<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtPhoto extends Model
{
    protected $fillable = ['art_id','name','path'];

    public function art()
    {
        return $this->belongsTo(Art::class);
    }

}
