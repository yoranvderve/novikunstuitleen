<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'street',
        'number',
        'suffix',
        'zip_code',
        'city',
        'country'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
