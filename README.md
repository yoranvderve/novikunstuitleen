# NoviKunstuitleen

Welkom bij de repository voor mijn eindopdracht van Praktijk 2

In deze README leg ik uit hoe je het project zelf lokaal werkend kunt krijgen

**Pre requirements**

Het is belangrijk dat je een lokale apache server en mysql database hebt geinstalleerd.

Je kunt hier het beste XAMPP of MAMP voor gebruiken. Deze tools bieden een lokale webserver en mysql

https://www.mamp.info/en/downloads/

https://www.apachefriends.org/download.html


Ook heb je composer en npm nodig om het project te compilen

https://getcomposer.org/download/

https://www.npmjs.com/get-npm


**Het project installeren**

Clone dit project naar de htdocs map van XAMPP of MAMP

`cd ~/pad-naar-htdocs-map`

`git clone https://gitlab.com/yoranvderve/novikunstuitleen.git`


Ga naar het project

`cd novikunstuitleen`


Installeer alle dependencies van het project

`composer install`

`npm install`


Compile alle CSS en Javascript

`npm run dev`

Als alles gecompiled is kun je de runner sluiten



Kopier het bestand .env.example naar een nieuw bestand: .env

Maak een nieuwe `APP_KEY` aan via de termial, in de projectmap:

`php artisan key:generate`

Wijzig de volgende waarden in `.env` in een teksteditor:

`DB_DATABASE=aangemaakte database voor dit project`

`DB_USER=root (of een andere aangemaakte gebruiker voor de database)`

`DB_PASSWORD=root of leeg of zelf aangemaakte wachtwoord`

```
NOTE:
Bij nieuwere versies van MAMP en XAMPP kan het nodig zijn om te verbinden met een database socket.
Maak dan een nieuwe variable DB_SOCKET aan met de volgende waarde:
MAMP: '/Applications/MAMP/tmp/mysql/mysql.sock'
XAMPP: '/Applications/XAMPP/xamppfiles/var/mysql/mysql.sock'

Dit pad kan afwijken per installatie en OS.
```


Maak een nieuwe variabele aan binnen dit bestand met de volgende waarde om Mollie werkend te krijgen:

`MOLLIE_KEY=test_VRhwG53uKk5874hgkfAAB6jgv6TjfR`


Als de database connectie goed is gegaan, moet je de volgende commando's draaien om de tabellen aan te maken in de database:

`php artisan migrate`


En dit commando om test data in de database te zetten

`php artisan db:seed`


Nu kun je in een browser naar de website gaan. `LET OP: je moet naar de public map toegaan om de website te benaderen!`

Veel voorkomende lokale urls (dit is afhankelijk van de instellingen van de lokale webserver):

* localhost/novikunstuitleen/public
* localhost:8888/novikunstuitleen/public
* localhost:8080/novikunstuitleen

```
NOTE:
Het kan voorkomen dat er een error optreed als je naar de website gaat met een soortgelijke melding:

The stream or file "/Applications/XAMPP/xamppfiles/htdocs/NoviKunstUitleenTest/storage/logs/laravel.log" could not be opened: failed to open stream: Permission denied

Wanneer dit gebeurt, moet je de `storage` map, plus alle onderliggende mappen + bestanden alle mogelijk rechten geven (chmod -R 777 storage)

Eenzelfde error kan zich voordoen als je een kunstwerk met afbeeldingen wil toevoegen.
Voer dan dezelfde actie uit op de map `public`
```

Als er testdata in de database staat, kun je inloggen met de volgende gegevens:
```
Admin
Gebruikersnaam: admin@gmail.com
Wachtwoord: password
```

```
Medewerker
Gebruikersnaam: medewerker@gmail.com
Wachtwoord: password
```

```
Student
Gebruikersnaam: student@gmail.com
Wachtwoord: password
```

**note*
Er zijn nog geen kunstwerken in de testdatabase aangemaakt. Dit kun je zelf doen door als admin of als medewerker in te loggen en de kunstwerken aan te maken