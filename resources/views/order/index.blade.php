@extends('layouts.app')

@section('content')
    <!-- MAIN CONTENT-->

    <div class="section__content student_art_section section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <table class="table">
                        <thead>
                            <tr>
                              <th>Object</th>
                              <th>Price</th>
                              <th>Rented On</th>
                              <th>Rented Till</th>
                              <th>Rented By</th>
                              <th>Send back</th>
                              <th>Payment ID</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orders as $order)
                              <tr>
                                  <td>{{$order->art->name}}</td>
                                  <td>&euro; {{$order->price}}</td>
                                  <td>{{Carbon\Carbon::parse($order->created_at)->format('d-m-Y')}}</td>
                                  <td>{{Carbon\Carbon::parse($order->created_at)->addMonths($order->amount_of_months)->format('d-m-Y')}}</td>
                                  <td>{{$order->user->name}}</td>
                                  <td>
                                      @if($order->send_back)
                                          yes
                                      @else
                                          no
                                      @endif
                                  </td>
                                  <td>{{$order->payment_id}}</td>
                              </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
