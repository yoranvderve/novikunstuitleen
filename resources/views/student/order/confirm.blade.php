@extends('layouts.app')

@section('content')
    <!-- MAIN CONTENT-->

    <div class="section__content student_art_section section__content--p30">
        <div class="container-fluid">
            <div class="row">
                  <div class="col-8 offset-2">
                      <div class="card">
                          <div class="card-header">
                              @if($order->paid)
                                Successfully Ordered!
                              @else
                                Oops! Something went wrong during payment!
                              @endif
                          </div>
                          <div class="card-body">
                              @if($order->paid)
                                Thank you for renting art!<br><br>
                                You ordered the following product:<br>
                                {{$order->art->name}}<br><br>
                                We are working hard to deliver the product as soon as possible!<br>
                                Usually this will take up to 3 working days.<br><br>
                                Cheers!
                              @else
                                Unfortunately we could not confirm the payment!<br><br>
                                Please place a new order <a href="{{route('student.art.rent', $order->art)}}">here</a>
                              @endif
                          </div>
                      </div>
                  </div>
            </div>
        </div>
    </div>
@endsection
