@extends('layouts.app')

@section('content')
    <!-- MAIN CONTENT-->

    <div class="section__content student_art_show_section section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-8 offset-2">
                    <div class="card">
                        <div class="card-header">
                            Rent {{$art->name}}
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-8">
                                  {{Form::open(['route'=>['student.art.confirm', $art], 'method'=>'post'])}}
                                    <h5>Address information</h5>
                                    <div class="form-group">
                                      Choose a saved address<br>
                                      <select name="address_select">
                                        @forelse($addresses as $address)
                                          <option value="{{$address->id}}">{{$address->street}} {{$address->number}} {{$address->suffix}}</option>
                                        @empty
                                          <option value="null">No saved address available</option>
                                        @endforelse
                                      </select>
                                    </div>
                                    Or create a new address
                                    <div class="form-group">
                                        <input class="form-control" name="street" placeholder="Street"/>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" name="number" placeholder="Number"/>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" name="suffix" placeholder="Suffix"/>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" name="zip_code" placeholder="Zip Code"/>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" name="city" placeholder="City"/>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" name="country" placeholder="Country"/>
                                    </div>

                                    <h6>For how long do you want to rent this object?</h6>
                                    <select name="period_select">
                                      <option value="3">3 Months</option>
                                      <option value="4">4 Months</option>
                                      <option value="5">5 Months</option>
                                      <option value="6">6 Months</option>
                                      <option value="7">7 Months</option>
                                      <option value="8">8 Months</option>
                                      <option value="9">9 Months</option>
                                      <option value="10">10 Months</option>
                                      <option value="11">11 Months</option>
                                      <option value="12">12 Months</option>
                                    </select>
                                    <div class="form-group mt-5">
                                      <button class="btn btn-primary" type="submit">Confirm</button>
                                    </div>
                                    {{Form::close()}}
                                </div>
                                <div class="col-4">
                                    <h5>Art information</h5>
                                    @if($art->artPhotos()->first() !== null)
                                    <img src="{{asset($art->artPhotos()->first()->path.'/'.$art->artPhotos()->first()->name)}}"/>
                                    @endif
                                    <h6>Title: {{$art->name}}</h6>
                                    <h6>Price: &euro;{{$art->price}}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
