@extends('layouts.app')

@section('content')
    <!-- MAIN CONTENT-->

    <div class="section__content student_art_show_section section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                  <h1>{{$art->name}} <a class="btn btn-primary" href="{{route('student.art.rent', $art)}}">Rent Art</a></h1>
                  <h3>Photos</h3>
                  <div class="row">
                    @forelse($art->artPhotos as $photo)
                      <div class="col-6">
                          <img src="{{asset($photo->path.'/'.$photo->name)}}"/>
                      </div>
                    @empty
                    No Photos available
                    @endforelse
                  </div>
                  <h5>Material: {{$art->material}}</h5>
                  <h5>Price: &euro;{{$art->price}}</h5>

                  <h3>Description</h3>
                  <p>{{$art->description}}</p>
                </div>
            </div>
        </div>
    </div>
@endsection
