@extends('layouts.app')

@section('content')
    <!-- MAIN CONTENT-->

    <div class="section__content student_art_section section__content--p30">
        <div class="container-fluid">
            <div class="row">
                    @foreach($artworks as $art)
                    <div class="col-6">
                        <div class="card art-card">
                            <div class="card-header">
                                {{$art->name}}
                            </div>
                            <div class="card-body">
                                <div class="image">
                                    @if($art->artPhotos()->first() != null)
                                      <img src="{{asset($art->artPhotos()->first()->path.'/'.$art->artPhotos()->first()->name)}}"/>
                                    @else
                                      No Preview Available
                                    @endif
                                </div>
                                <div class="price">
                                  Price: &euro;{{$art->price}}
                                </div>
                            </div>
                            <div class="card-footer">
                                <a class="btn btn-primary" href="{{route('student.art.show', $art)}}">Show</a>
                            </div>
                        </div>
                      </div>
                    @endforeach
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $(document).ready(function(){
            let height = 0;
            $('.art-card').each(function(){
                if($(this).height() > height) {
                  height = $(this).height();
                }
            });
            height = height + 30;
            $('.art-card').each(function(){
              $(this).css('height', height);
            });
        });
    </script>
@endsection
