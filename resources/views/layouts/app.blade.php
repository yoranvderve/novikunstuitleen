<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('CoolAdmin-master/js/main.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('CoolAdmin-master/css/theme.css') }}" rel="stylesheet">
    <link href="{{ asset('CoolAdmin-master/css/font-face.css') }}" rel="stylesheet">

    <!-- Theme -->
    <link href="{{ asset('CoolAdmin-master/vendor/animsition/animsition.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('CoolAdmin-master/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css')  }}" rel="stylesheet" media="all">
    <link href="{{ asset('CoolAdmin-master/vendor/wow/animate.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('CoolAdmin-master/vendor/css-hamburgers/hamburgers.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('CoolAdmin-master/vendor/slick/slick.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('CoolAdmin-master/vendor/select2/select2.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('CoolAdmin-master/vendor/perfect-scrollbar/perfect-scrollbar.css') }}" rel="stylesheet" media="all">

</head>
<body class="animsition">
<div class="page-wrapper">
    <!-- HEADER MOBILE-->
    <header class="header-mobile d-block d-lg-none">
        <div class="header-mobile__bar">
            <div class="container-fluid">
                <div class="header-mobile-inner">
                    <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                    </button>
                </div>
            </div>
        </div>
        @include('layouts.mobile-sidebar')
    </header>
    <!-- END HEADER MOBILE-->

    <!-- MENU SIDEBAR-->
    <aside class="menu-sidebar d-none d-lg-block">
        <div class="logo">
            <a href="{{route('home')}}">
                <img src="{{ asset('art-logo.jpg') }}" alt="Novi Kunstuitleen Logo" width="86px"/>
            </a>
        </div>
        <div class="menu-sidebar__content js-scrollbar1">
            @include('layouts.sidebar')
        </div>
    </aside>
    <!-- END MENU SIDEBAR-->

    <!-- PAGE CONTAINER-->
    <div class="page-container">
        <!-- HEADER DESKTOP-->
        @include('layouts.nav')
        <!-- HEADER DESKTOP-->
    <div class="main-content">
        <div class="container">
          <div class="row">
            <div class="col-12">
              @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
            </div>
            @endif


            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
              </div>
            @endif


            @if ($message = Session::get('warning'))
            <div class="alert alert-warning alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{ $message }}</strong>
            </div>
            @endif


            @if ($message = Session::get('info'))
            <div class="alert alert-info alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{ $message }}</strong>
            </div>
            @endif


            @if ($errors->any())
            <div class="alert alert-danger">
              <button type="button" class="close" data-dismiss="alert">×</button>
              Please check the form below for errors
            </div>
            @endif
            </div>
          </div>
        </div>
            @yield('content')
        </div>

        <!-- END PAGE CONTAINER-->
    </div>

</div>

<!-- Jquery JS-->
<script src="{{ asset('CoolAdmin-master/vendor/jquery-3.2.1.min.js') }}"></script>
<!-- Bootstrap JS-->
<script src="{{ asset('CoolAdmin-master/vendor/bootstrap-4.1/popper.min.js') }}"></script>
<script src="{{ asset('CoolAdmin-master/vendor/bootstrap-4.1/bootstrap.min.js') }}"></script>
<!-- Vendor JS       -->
<script src="{{ asset('CoolAdmin-master/vendor/slick/slick.min.js') }}">
</script>
<script src="{{ asset('CoolAdmin-master/vendor/wow/wow.min.js') }}"></script>
<script src="{{ asset('CoolAdmin-master/vendor/animsition/animsition.min.js') }}"></script>
<script src="{{ asset('CoolAdmin-master/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js') }}">
</script>
<script src="{{ asset('CoolAdmin-master/vendor/counter-up/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('CoolAdmin-master/vendor/counter-up/jquery.counterup.min.js') }}">
</script>
<script src="{{ asset('CoolAdmin-master/vendor/circle-progress/circle-progress.min.js') }}"></script>
<script src="{{ asset('CoolAdmin-master/vendor/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
<script src="{{ asset('CoolAdmin-master/vendor/chartjs/Chart.bundle.min.js') }}"></script>
<script src="{{ asset('CoolAdmin-master/vendor/select2/select2.min.js') }}">
</script>

<!-- Main JS-->
<script src="{{ asset('CoolAdmin-master/js/main.js') }}"></script>

@yield('javascript')

</body>
</html>
