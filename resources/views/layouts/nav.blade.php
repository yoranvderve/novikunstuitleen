<header class="header-desktop">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="header-wrap">
                <div class="header-button account-button">
                    <div class="account-wrap">
                        <div class="account-item clearfix js-item-menu">
                            <div class="image">
                                {{--                                <img src="{{ asset('CoolAdmin-master/images/icon/avatar-02.jpg') }}images/icon/avatar-01.jpg" alt="John Doe" />--}}
                            </div>
                            <div class="content">
                                @if(Auth::check())
                                    <a class="js-acc-btn" href="#">{{Auth::user()->name}}</a>
                                @else
                                    <a href="{{route('login')}}">My Account</a>

                                @endif
                            </div>

                            <div class="account-dropdown js-dropdown">
                                <div class="info clearfix">
                                    @if(Auth::check())
                                        <div class="content">
                                            <h5 class="name">
                                                <a href="#">{{Auth::user()->name}}</a>
                                            </h5>
                                            <span class="email">{{Auth::user()->email}}</span>
                                        </div>
                                    @endif
                                </div>
                                @if(Auth::check())

                                <div class="account-dropdown__body">
                                        <div class="account-dropdown__item">
                                            <a href="@if(Auth::user()->role === 'student'){{route('student.order.index')}}@else{{route('order.index')}}@endif">
                                                <i class="zmdi zmdi-money-box"></i>Orders</a>
                                        </div>
                                </div>
                                <div class="account-dropdown__footer">
                                    <a href="{{route('logout')}}">
                                        <i class="zmdi zmdi-power"></i>Logout</a>
                                </div>
                                @else
                                    <div class="account-dropdown__footer">
                                        <a href="{{route('login')}}">
                                            <i class="zmdi zmdi-power"></i>Inloggen</a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
