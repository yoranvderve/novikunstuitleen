@if(Auth::check())
    <nav class="navbar-mobile">
        <div class="container-fluid">
            <ul class="navbar-mobile__list list-unstyled">
              @if(Auth::user()->role->value === 'admin' || Auth::user()->role->value === 'employee')
              <li>
                  <a href="{{route('art.index')}}">
                      <i class="fas fa-table"></i>Artworks @if(Auth::user()->role->value === 'admin') (employee) @endif</a>
              </li>
              <li>
                  <a href="{{route('order.index')}}">
                      <i class="fas fa-table"></i>Orders</a>
              </li>
              @endif
              @if(Auth::user()->role->value === 'admin' || Auth::user()->role->value === 'student')
              <li>
                  <a href="{{route('student.art.index')}}">
                      <i class="fas fa-table"></i>Artworks @if(Auth::user()->role->value === 'admin') (student) @endif</a>
              </li>
              <li>
                  <a href="{{route('student.order.index')}}">
                      <i class="fas fa-table"></i>My Orders</a>
              </li>
              @endif
            </ul>
        </div>
    </nav>
@endif
