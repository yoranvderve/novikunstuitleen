@extends('layouts.app')

@section('content')
    <!-- MAIN CONTENT-->

    <div class="section__content art_edit_section section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-7">
                  {{Form::open(['route'=>['art.update', $art->id], 'method'=>'put', 'enctype' => 'multipart/form-data'])}}
                        <div class="card">
                            <div class="card-header">
                                <small>Edit </small>
                                <strong>{{$art->name}}</strong>
                            </div>
                            <div class="card-body card-block">
                                @include('art.form')
                            </div>
                            <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Submit
                            </button>
                        </div>
                    {{Form::close()}}
                </div>
                <div class="col-5">
                    @foreach($photos as $photo)
                      <div class="row">
                        <div class="col-10">
                          <div class="images">
                            <img src="{{asset($photo->path.'/'.$photo->name)}}"/>
                          </div>
                        </div>
                        <div class="col-2">
                          {{Form::open(['route'=>['art.photo.delete', $photo], 'method'=>'delete'])}}
                          <button type="submit" class="btn btn-danger" onclick="return confirm('are you sure you want to delete this photo?')">delete</button>
                        </div>
                      </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
