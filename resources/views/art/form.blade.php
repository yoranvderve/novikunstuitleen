<div class="form-group">
    <label for="name" class=" form-control-label">Name</label>
    <input type="text" id="name" name="name" placeholder="Enter the name of the artwork" class="form-control" @if(isset($art->id)) value="{{$art->name}}" @endif required>
</div>
<div class="form-group">
    <label for="material" class=" form-control-label">Material</label>
    <input type="text" id="material" name="material" placeholder="Enter the material" class="form-control" @if(isset($art->id)) value="{{$art->material}}" @endif required>
</div>
<div class="form-group">
    <label for="description" class=" form-control-label">Description</label>
    <textarea id="description" name="description" placeholder="Enter the description" class="form-control" required>@if(isset($art->id)){{$art->description}}@endif</textarea>
</div>
<div class="form-group">
    <label for="price" class=" form-control-label">Price</label>
    <input type="text" id="price" name="price" placeholder="Enter the price in euros" class="form-control" @if(isset($art->id)) value="{{$art->price}}" @endif required>
</div>
<div class="form-group">
    <label for="pictures" class=" form-control-label">Pictures</label>
    <input type="file" id="pictures" name="pictures[]" class="form-control" multiple>
</div>
