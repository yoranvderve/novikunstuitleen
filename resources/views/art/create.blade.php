@extends('layouts.app')

@section('content')
    <!-- MAIN CONTENT-->

    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-7">
                    <form action="{{route('art.store')}}" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="card">
                        <div class="card-header">
                            <small>Upload new </small>
                            <strong>Artwork!</strong>
                        </div>
                        <div class="card-body card-block">
                            @include('art.form')
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
