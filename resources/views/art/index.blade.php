@extends('layouts.app')

@section('content')
    <!-- MAIN CONTENT-->

    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            Overview artworks
                            <a href="{{route('art.create')}}" class="btn btn-primary float-right">Add new artwork</a>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                    <td>Name</td>
                                    <td>Price</td>
                                    <td>Material</td>
                                    <td>Status</td>
                                    <td>#</td>
                                    <td>#</td>
                                </thead>
                                <tbody>
                                @foreach($artworks as $artwork)
                                    <tr>
                                        <td>{{$artwork->name}}</td>
                                        <td>&euro;{{$artwork->price}}</td>
                                        <td>{{$artwork->material}}</td>
                                        <td>{{$artwork->status}}</td>
                                        <td><a href="{{route('art.edit',$artwork->id)}}">edit</a></td>
                                        <td>
                                            {{Form::open(['route'=>['art.destroy', $artwork], 'method'=>'delete'])}}
                                              <button class='btn btn-danger' type="submit" onclick="return confirm('are you sure?')">delete</button>
                                            {{Form::close()}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
