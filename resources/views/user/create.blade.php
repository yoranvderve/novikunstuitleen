@extends('layouts.app')

@section('content')
    <!-- MAIN CONTENT-->

    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-7">
                    <form action="{{route('user.store')}}" method="post">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <small>Create new </small>
                            <strong>User!</strong>
                        </div>
                        <div class="card-body card-block">
                            @include('user.form')
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
