@extends('layouts.app')

@section('content')
    <!-- MAIN CONTENT-->

    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            Overview Users
                            <a href="{{route('user.create')}}" class="btn btn-primary float-right">Add new user</a>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                    <td>Name</td>
                                    <td>Email</td>
                                    <td>Role</td>
                                    <td>Added on</td>
                                    <td>Rented artworks</td>
                                    <td>#</td>
                                    <td>#</td>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->role->name}}</td>
                                        <td>{{Carbon\Carbon::parse($user->created_at)->format('d-m-Y')}}</td>
                                        <td>{{$user->orders()->where('paid',true)->count()}}</td>
                                        <td><a href="{{route('user.edit',$user->id)}}">edit</a></td>
                                        <td>
                                            {{Form::open(['route'=>['user.destroy', $user], 'method'=>'delete'])}}
                                              <button class='btn btn-danger' type="submit" onclick="return confirm('are you sure?')">delete</button>
                                            {{Form::close()}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
