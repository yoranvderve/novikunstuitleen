<div class="form-group">
    <label for="name" class=" form-control-label">Name</label>
    <input type="text" id="name" name="name" placeholder="Enter the name of the artwork" class="form-control" @if(isset($user->id)) value="{{$user->name}}" @endif>
</div>
<div class="form-group">
    <label for="email" class=" form-control-label">Email</label>
    <input type="email" id="email" name="email" placeholder="Enter the email" class="form-control" @if(isset($user->id)) value="{{$user->email}}" @endif>
</div>
<div class="form-group">
    <label for="password" class=" form-control-label">Password</label>
    <input type="password" id="password" name="password" placeholder="Enter the (new) password" class="form-control">
</div>
<div class="form-group">
    <label for="role" class=" form-control-label">Role</label>
    <select id="role" name="role" class="form-control">
        <option value="2">Employee</option>
        <option value="3">Student</option>
    </select>
</div>
