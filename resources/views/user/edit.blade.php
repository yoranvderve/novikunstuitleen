@extends('layouts.app')

@section('content')
    <!-- MAIN CONTENT-->

    <div class="section__content art_edit_section section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-7">
                  {{Form::open(['route'=>['user.update', $user->id], 'method'=>'put',])}}
                        <div class="card">
                            <div class="card-header">
                                <small>Edit </small>
                                <strong>{{$user->name}}</strong>
                            </div>
                            <div class="card-body card-block">
                                @include('user.form')
                            </div>
                            <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Submit
                            </button>
                        </div>
                    {{Form::close()}}
                </div>
                <div class="col-4">
                    <h4>Saved addresses</h4>
                    @foreach($user->addresses as $address)
                        <div class="card">
                            <div class="card-body">
                                {{$address->street}} {{$address->number}} {{$address->suffix}}<br>
                                {{$address->zip_code}}<br>
                                {{$address->city}}<br>
                                {{$address->country}}<br>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
