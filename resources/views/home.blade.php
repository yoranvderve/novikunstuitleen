@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Welcome!</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(Auth::check())
                        You are logged in!
                    @else
                            Please <a href="{{route('login')}}">log in</a> to use this application
                        @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
